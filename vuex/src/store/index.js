import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listData: [
      {
        name: 'Item 1'
      },
      {
        name: 'Item 2'
      },
      {
        name: 'Item 3'
      }
    ]
  },
  mutations: {
    setData(state, payload) {
      state.listData.push({name: payload})
    },
    deleteData(state, payload) {
      state.listData.splice(payload, 1)
    },
    updateData(state, payload) {
      state.listData[payload.index].name = payload.item
    },


  },
  actions: {
  },
  modules: {
  }
})
